const express = require("express");
const app = express();
const rotaEscolas = require("./api/routes/escolas");
const rotaAulas = require("./api/routes/aulas");
const rotaInstrumentos = require("./api/routes/instrumentos");
const rotaProfessores = require("./api/routes/professores");
const rotaAlunos = require("./api/routes/alunos");
const mongoose = require('./api/data');

app.use(express.json());

app.use("/escolas", rotaEscolas);
app.use("/aulas", rotaAulas);
app.use("/instrumentos", rotaInstrumentos);
app.use("/professores", rotaProfessores);
app.use("/alunos", rotaAlunos);

mongoose.connection.on('connected', () => {
    app.listen(3000, () => {
        console.log('Aplicação executando na porta 3000');
    });
})