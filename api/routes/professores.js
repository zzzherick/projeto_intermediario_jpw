const express = require("express");
const router = express.Router();
const Professor = require('../models/Professor')

// GET ALL
router.get("/", (req, res) => {   
    Professor.find((err, doc) => {
        if(!err) {
            if (req.query.limit){
                var limitador = req.query.limit;
                limitador--;
                
                var result = [];
                while(limitador >= 0){
                    if(doc[limitador]){
                        result.push(doc[limitador]);
                    }
                    
                    limitador--;
                }

                res.json(result)
                
            } else{
                res.json(doc);
            }
        }
    })
});

// GET ONE
router.get("/:id", (req, res) => {

    let parametro_id = req.params.id

    Professor.findOne({ _id: parametro_id}, (err, doc) => {
        if(!err){
            res.json(doc)
        } else {
            console.error(err)
        }
    })
});

// POST
router.post("/", (req, res) => {

    const conteudo = req.body;  
    var novoProfessor = new Professor(conteudo);
    res.json(novoProfessor)
    novoProfessor.save();
});

// PUT
router.put('/:id', (req, res) => {
    
    let parametro_id = req.params.id
    let updateProfessor = req.body

    Professor.findOneAndUpdate({ _id: parametro_id }, updateProfessor, (err, doc) => {
        if(!err) {
            res.json(updateProfessor)
        } else{
            console.error(err)
        }
    })
});

// DELETE
router.delete('/:id', (req, res) => {
    
    let param_id = req.params.id
    Professor.findByIdAndDelete(param_id, (err, doc) => {
        if(!err){
            res.json(doc)
        }
    })
});

module.exports = router;