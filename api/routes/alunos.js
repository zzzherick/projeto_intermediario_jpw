const express = require("express");
const router = express.Router();
const Aluno = require("../models/Aluno")

// GET ALL
router.get("/", (req, res) => {   
    Aluno.find((err, doc) => {
        if(!err) {
            if (req.query.limit){
                var limitador = req.query.limit;
                limitador--;
                
                var result = [];
                while(limitador >= 0){
                    if(doc[limitador]){
                        result.push(doc[limitador]);
                    }
                    
                    limitador--;
                }

                res.json(result)
                
            } else{
                res.json(doc);
            }
        }
    })
});

// GET ONE
router.get("/:id", (req, res) => {

    let parametro_id = req.params.id

    Aluno.findOne({ _id: parametro_id }, (err, doc) => {
        if(!err){
            res.json(doc)
        } else{
            console.error(err)
        }
    })
});

// POST
router.post("/", (req, res) => {
    const conteudo = req.body
    var novoAluno = new Aluno(conteudo)
    res.json(novoAluno)
    novoAluno.save()
});

// PUT
router.put('/:id', (req, res) => {
    
    let parametro_id = req.params.id
    let updateAluno = req.body

    Aluno.findOneAndUpdate({ _id: parametro_id }, updateAluno, (err, doc) => {
        if(!err) {
            res.json(updateAluno)
        } else{
            console.error(err)
        }
    })
});

// DELETE
router.delete('/:id', (req, res) => {

    let param_id = req.params.id
    Aluno.findByIdAndDelete(param_id, (err, doc) => {
        if(!err){
            res.json(doc)
        }
    })
});

module.exports = router;