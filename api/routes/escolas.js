const express = require("express");
const router = express.Router();
//const evento = require("../data/evento");
const Escola = require('../models/Escola')

// GET ALL
router.get("/", (req, res) => {   
    //OLD MODE
    //res.json(evento.getEvento().escola);
    
    Escola.find((err, doc) => {
        if(!err) {
            if (req.query.limit){
                var limitador = req.query.limit;
                limitador--;
                
                var result = [];
                while(limitador >= 0){
                    if(doc[limitador]){
                        result.push(doc[limitador]);
                    }
                    
                    limitador--;
                }

                res.json(result)
                
            } else{
                res.json(doc);
            }
        }
    })
});

// GET ONE
router.get("/:id", (req, res) => {
    
    let parametro_id = req.params.id

    Escola.findOne({ _id: parametro_id}, (err, doc) => {
        if(!err){
            res.json(doc)
        } else {
            console.error(err)
        }
    })

    // OLD MODE
    /*
    let escolasComId = evento.getEvento().escola.filter(e => e._id == req.params.id);

    if(escolasComId.length < 1){
        res.json({});
    }else{
        res.json(escolasComId[0]);
    }
    */
});

// POST
router.post("/", (req, res) => {
    
    //OLD MODE
    /*const e = evento.getEvento();
    const newEscola = req.body;  

    e.escola.push(newEscola);
    evento.saveEvento(e);

    res.json(newEscola);
    */

    const conteudo = req.body;  
    var novaEscola = new Escola(conteudo);
    res.json(novaEscola)
    novaEscola.save();
});

// PUT
router.put('/:id', (req, res) => {
    
    let parametro_id = req.params.id
    let updateEscola = req.body

    Escola.findOneAndUpdate({ _id: parametro_id }, updateEscola, (err, doc) => {
        if(!err) {
            res.json(updateEscola)
        } else{
            console.error(err)
        }
    })
    // OLD MODE
    /*const e = evento.getEvento();
    const updateEscola = req.body;

    let pos = e.escola.findIndex( e => e._id == req.params.id);

    if(pos < 0){
        res.json({});
    }else{
        e.escola[pos] = updateEscola;
        evento.saveEvento(e);
        res.json(updateEscola);
    }
    */
});

// DELETE
router.delete('/:id', (req, res) => {

    let param_id = req.params.id
    Escola.findByIdAndDelete(param_id, (err, doc) => {
        if(!err){
            res.json(doc)
        }
    })

    // OLD MODE
    /*
    const e = evento.getEvento();
    let pos = e.escola.findIndex( e => e._id == req.params.id);

    if(pos < 0){
        res.json({});
    }else{
        const anterior = e.escola[pos];
        e.escola.splice(pos, 1);

        evento.saveEvento(e);
        res.json(anterior);
    }
    */
});

module.exports = router;