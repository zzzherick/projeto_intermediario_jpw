const express = require("express");
const router = express.Router();
const Aula = require('../models/Aula')

// GET ALL
router.get("/", (req, res) => {   
    Aula.find((err, doc) => {
        if(!err) {
            if (req.query.limit){
                var limitador = req.query.limit;
                limitador--;
                
                var result = [];
                while(limitador >= 0){
                    if(doc[limitador]){
                        result.push(doc[limitador]);
                    }
                    
                    limitador--;
                }

                res.json(result)
                
            } else{
                res.json(doc);
            }
        }
    })
});

// GET ONE
router.get("/:id", (req, res) => {

    let parametro_id = req.params.id

    Aula.findOne({ _id: parametro_id}, (err, doc) => {
        if(!err){
            res.json(doc)
        } else {
            console.error(err)
        }
    })
});

// POST
router.post("/", (req, res) => {
    const conteudo = req.body;  
    var novaAula = new Aula(conteudo);
    res.json(novaAula)
    novaAula.save();
});

// PUT
router.put('/:id', (req, res) => {
    let parametro_id = req.params.id
    let updateAula = req.body

    Aula.findOneAndUpdate({ _id: parametro_id }, updateAula, (err, doc) => {
        if(!err) {
            res.json(updateAula)
        } else{
            console.error(err)
        }
    })
});

// DELETE
router.delete('/:id', (req, res) => {
    
    let param_id = req.params.id
    Aula.findByIdAndDelete(param_id, (err, doc) => {
        if(!err){
            res.json(doc)
        }
    })
});

module.exports = router;