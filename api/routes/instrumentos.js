const express = require("express");
const router = express.Router();
const Instrumento = require('../models/Instrumento')

// GET ALL
router.get("/", (req, res) => {   
    Instrumento.find((err, doc) => {
        if(!err) {
            if (req.query.limit){
                var limitador = req.query.limit;
                limitador--;
                
                var result = [];
                while(limitador >= 0){
                    if(doc[limitador]){
                        result.push(doc[limitador]);
                    }
                    
                    limitador--;
                }

                res.json(result)
                
            } else{
                res.json(doc);
            }
        }
    })
});

// GET ONE
router.get("/:id", (req, res) => {

    let parametro_id = req.params.id

    Instrumento.findOne({ _id: parametro_id}, (err, doc) => {
        if(!err){
            res.json(doc)
        } else {
            console.error(err)
        }
    })
});

// POST
router.post("/", (req, res) => {
    const conteudo = req.body;
    var novoInstrumento = new Instrumento(conteudo);
    res.json(novoInstrumento)
    novoInstrumento.save();
});

// PUT
router.put('/:id', (req, res) => {

    let parametro_id = req.params.id
    let updateInstrumento = req.body

    Instrumento.findOneAndUpdate({ _id: parametro_id }, updateInstrumento, (err, doc) => {
        if(!err) {
            res.json(updateInstrumento)
        } else{
            console.error(err)
        }
    })
});

// DELETE
router.delete('/:id', (req, res) => {
    
    let param_id = req.params.id
    Instrumento.findByIdAndDelete(param_id, (err, doc) => {
        if(!err){
            res.json(doc)
        }
    })
});

module.exports = router;