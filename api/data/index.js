const mongoose = require("mongoose")

var url = "mongodb://localhost:27017/musicas"
var options = {useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false}

mongoose.connect(url, options)

module.exports = mongoose