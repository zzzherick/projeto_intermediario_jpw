const mongoose = require('../data')

var instrumentoScheme = new mongoose.Schema({
    'marca': String,
    'nome_popular': String,
    'criadoEm': { type: Date, default: Date.now}
})

var Instrumento = mongoose.model('Instrumento', instrumentoScheme)

module.exports = Instrumento