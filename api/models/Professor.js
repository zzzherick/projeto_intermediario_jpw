const mongoose = require('../data')

var professorScheme = new mongoose.Schema({
    'nome': String,
    'ano_nascimento': Number,
    'salario': Number,
    'criadoEm': { type: Date, default: Date.now}
})

var Professor = mongoose.model('Professor', professorScheme)

module.exports = Professor