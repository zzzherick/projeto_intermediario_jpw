const mongoose = require('../data')

var escolaScheme = new mongoose.Schema({
    'nome': String,
    'local': String,
    'criadoEm': { type: Date, default: Date.now}
})

var Escola = mongoose.model('Escola', escolaScheme)

module.exports = Escola