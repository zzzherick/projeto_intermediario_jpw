const mongoose = require('../data')

var aulaScheme = new mongoose.Schema({
    'nome': String,
    'duracao': String,
    'mensalidade': Number,
    'criadoEm': { type: Date, default: Date.now}
})

var Aula = mongoose.model('Aula', aulaScheme)

module.exports = Aula