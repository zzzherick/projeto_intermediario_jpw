const mongoose = require('../data')

var alunoScheme = new mongoose.Schema({
    'nome': String,
    'ano_nascimento': Number,
    'criadoEm': { type: Date, default: Date.now}
})

var Aluno = mongoose.model('Aluno', alunoScheme)

module.exports = Aluno