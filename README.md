**Projeto Intermediario - Java P/ Web ( UNESC )**

A ideia foi fazer um CRUD de Escola de Música, utilizando as ferramentas apresentadas durante as aulas de Java p/ Web.

Como "recursos" do CRUD, teríamos: Escola; Aula; Instrumento; Professor; Aluno.

**ESCOLA**

- Para CADASTRAR (POST) uma nova ESCOLA utilize a rota /escolas/
{
    "nome": "nome",
    "local": "local"
}

- Para EDITAR (PUT) uma ESCOLA utilize a rota /escolas/id
{
    "nome": "nome",
    "local": "local"
}

- Para LISTAR TODAS as ESCOLAS (GET 'ALL') utilize a rota /escolas/
- Para LISTAR apenas UMA ESCOLA (GET 'ONE') utilize a rota /escolas/id
- Para DELETAR uma ESCOLA (DELETE) utilize a rota /escolas/id

**AULA**

- Para CADASTRAR (POST) uma nova AULA utilize a rota /aulas/
{
    "nome": "nome",
    "duracao": "duracao_string",
    "mensalidade": "valor_float"
}

- Para EDITAR (PUT) uma AULA utilize a rota /aulas/id
{
    "duracao": "duracao_string",
    "mensalidade": "valor_float"
}

- Para LISTAR TODAS as AULAS (GET 'ALL') utilize a rota /aulas/
- Para LISTAR apenas UMA AULA (GET 'ONE') utilize a rota /aulas/id
- Para DELETAR uma AULA (DELETE) utilize a rota /aulas/id

**INSTRUMENTO**

- Para CADASTRAR (POST) um novo INSTRUMENTO utilize a rota /instrumentos/
{
    "marca": "marca",
    "nome_popular": "nomepopular"
}

- Para EDITAR (PUT) um INSTRUMENTO utilize a rota /instrumentos/id
{
    "marca": "marca",
    "nome_popular": "nomepopular"
}

- Para LISTAR TODOS os INSTRUMENTOS (GET 'ALL') utilize a rota /instrumentos/
- Para LISTAR apenas UM INSTRUMENTO (GET 'ONE') utilize a rota /instrumentos/id
- Para DELETAR um INSTRUMENTO (DELETE) utilize a rota /instrumentos/id

**PROFESSOR**

- Para CADASTRAR (POST) um novo PROFESSOR utilize a rota /professores/
{
    "nome": "nome",
    "ano_nascimento": "anonascimento",
    "salario": "valor_float"
}

- Para EDITAR (PUT) um PROFESSOR utilize a rota /professores/id
{
    "nome": "nome",
    "ano_nascimento": "anonascimento",
    "salario": "valor_float"
}

- Para LISTAR TODOS os PROFESSORES (GET 'ALL') utilize a rota /professores/
- Para LISTAR apenas UM PROFESSOR (GET 'ONE') utilize a rota /professores/id
- Para DELETAR um PROFESSOR (DELETE) utilize a rota /professores/id

**ALUNO**

- Para CADASTRAR (POST) um novo ALUNO utilize a rota /alunos/
{
    "nome": "nome",
    "ano_nascimento": "anonascimento"
}

- Para EDITAR (PUT) um ALUNO utilize a rota /alunos/id
{
    "nome": "nome",
    "ano_nascimento": "anonascimento"
}

- Para LISTAR TODOS os ALUNOS (GET 'ALL') utilize a rota /alunos/
- Para LISTAR apenas UM ALUNO (GET 'ONE') utilize a rota /alunos/id
- Para DELETAR um ALUNO (DELETE) utilize a rota /alunos/id
